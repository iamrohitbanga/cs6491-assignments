// Project 1 (Part A): CS 6491
// Author(s): Rohit Banga, Teresa Lee, Wing Lok, David Stolarsky
// Subject: Two Player Game to arrange circles

int ncircles;
Circle[] player_1;
Circle[] player_2;

int turn;
int selected;
boolean mouse_released;
int interval;

Circle p1MinCover;
Circle p2MinCover;

VirtualPlayer vplayer1;
VirtualPlayer vplayer2;


void setup() {
  size(SCREENX, SCREENY);
  mouse_released = true;
  selected = -1;
  turn = 1;
  initCircles();
}

void initCircles() {
  String[] lines;
  lines = loadStrings(input_file);
 
  /*for(int l = 0; l < lines.length; l++)
  {
    lines[l] = "" + random(10.0, 40.0);
  }
 */

  int count = Integer.parseInt(lines[0]);
  println(count);
  player_1 = new Circle[count];
  player_2 = new Circle[count];

  ncircles = count;
  float current_y = buffer_space;
  for (int i = 0; i < ncircles; ++i) {
    float radius = float(lines[i+1]);
    player_1[i] = new Circle();
    player_1[i].center = new PVector(SCREENX/4 - buffer_space - radius, current_y + radius);
    player_1[i].radius = radius;
    player_1[i].color_code = color(random(255), random(255), int(current_y));

    player_2[i] = new Circle();
    player_2[i].center = new PVector(3*SCREENX/4 + buffer_space + radius, current_y + radius);
    player_2[i].radius = radius;
    player_2[i].color_code = player_1[i].color_code;

    current_y = current_y + 2*radius + buffer_space;

  }
  smooth();

  vplayer1 = null;
//  vplayer2 = new VirtualPlayerExample(player_2, new PVector(SCREENX/2, 0), new PVector(SCREENX, SCREENY));
//  vplayer2 = new SatsPlayer(player_2, new PVector(SCREENX/2, 0), new PVector(SCREENX, SCREENY));
  vplayer2 = new AutoSatsPlayer(player_2, new PVector(SCREENX/2, 0), new PVector(SCREENX, SCREENY));
}

void draw() {

  Circle[] circles = null;
  VirtualPlayer vplayer = null;
  // decide which set of circles to use depending on whose turn it is
  if (turn == 1) {
    circles = player_1;
    vplayer = vplayer1;
  }
  else if (turn == 3) {
    circles = player_2;
    vplayer = vplayer2;
  }

  if (vplayer == null) {
    PVector mouseVec = new PVector(mouseX, mouseY);
    findSelectedCircle(circles, mouseVec);
    moveCircle(circles, mouseVec);
  }
  else {
    //vplayer.arrangeCircles(p1MinCover.radius);
    vplayer.arrangeCirclesAutomatically();
  }

  if (turn == 1)
    p1MinCover = findMinimumDisk(circles); 
  else if (turn == 2)
    p2MinCover = findMinimumDisk(circles);
  else if (turn == 3)
    p2MinCover = findMinimumDisk(circles);

  render();
}

void findSelectedCircle(Circle[] circles, PVector mouseVec) {
  if (mousePressed && mouse_released) {   
    for (int i = 0; i < ncircles; ++i) {
      if (circles[i].center.dist(mouseVec) <= circles[i].radius) {
        selected = i;
        mouse_released = false;
        return;
      }
    }
  }
}

void moveCircle(Circle[] circles, PVector mouseVec) {
  if (selected < 0)
    return;

  List<Integer> colliding = new ArrayList<Integer>();
  for (int i = 0; i < ncircles; ++i) {
    if (i == selected)
      continue;
    float d = circles[i].center.dist(mouseVec);
    float R = circles[i].radius;
    float r = circles[selected].radius;
    if (d < (R+r)) {
      colliding.add(i);
    }
  }

  PVector newPosition = null;

  if (colliding.size() == 1) {
    int index = colliding.get(0);
    PVector direction = new PVector(mouseVec.x, mouseVec.y);
    direction.sub(circles[index].center);
    float magnitude = sqrt(direction.dot(direction));
    float R = circles[index].radius;
    float r = circles[selected].radius;
    direction.mult((R+r)/magnitude);

    newPosition = new PVector(circles[index].center.x, circles[index].center.y, 0);
    newPosition.add(direction);
  }
  else if (colliding.size() == 2) {
    int index1 = colliding.get(0);
    int index2 = colliding.get(1);
    float r = circles[selected].radius;
    float r1 = circles[index1].radius + r;
    float r2 = circles[index2].radius + r;
    float d = circles[index1].center.dist(circles[index2].center);
    float term1 = (sq(r2) - sq(r1)) / d;
    float term2 = d;
    float b = (term2 + term1) / 2;
    float a = (term2 - term1) / 2;
    PVector H = new PVector(circles[index1].center.x, circles[index1].center.y, 0);
    PVector C1C2 = new PVector(circles[index2].center.x - circles[index1].center.x,
                               circles[index2].center.y - circles[index1].center.y, 0);
    C1C2.mult(a/d);
    H.add(C1C2);

    PVector newP1 = new PVector(H.x, H.y, 0);
    float h = sqrt(sq(r1) - sq(a));
    PVector rotatedC1C2 = new PVector(-circles[index2].center.y + circles[index1].center.y, circles[index2].center.x - circles[index1].center.x, 0);
    rotatedC1C2.mult(h/d);
    newP1.add(rotatedC1C2);

    PVector newP2 = new PVector(H.x, H.y, 0);
    PVector rotatedC2C1 = new PVector(-circles[index1].center.y + circles[index2].center.y, circles[index1].center.x - circles[index2].center.x, 0);
    rotatedC2C1.mult(h/d);
    newP2.add(rotatedC2C1);

    if (circles[selected].center.dist(newP1) < circles[selected].center.dist(newP2))
      newPosition = newP1;
    else
      newPosition = newP2;
  }
  else if (colliding.size() == 0) {
    newPosition = mouseVec;
  }

  if (isPositionValid(circles, newPosition))
    circles[selected].center.set(newPosition);

}

// firewall to prevent all overlaps
boolean isPositionValid(Circle[] circles, PVector position) {

  if (position == null)
    return false;

  for (int i = 0; i < ncircles; ++i) {
    if (i == selected)
      continue;
    float d = circles[i].center.dist(position);
    float R = circles[i].radius;
    float r = circles[selected].radius;
    if (d < (R+r-1)) {
      return false;
    }
  }
  return true;
}

void render() {
  background(BACKGROUND);
  line(SCREENX/2, 0, SCREENX/2, SCREENY);
  noFill();
 
  if(p1MinCover != null)
    ellipse(p1MinCover.center.x, p1MinCover.center.y, p1MinCover.radius * 2, p1MinCover.radius * 2);
  //if(p2MinCover != null)
  //  ellipse(p2MinCover.center.x, p2MinCover.center.y, p2MinCover.radius * 2, p2MinCover.radius * 2);
  for (int i = 0; i < ncircles; ++i) {
    colorMode(HSB, 255);
    fill(player_1[i].color_code);
    ellipse(player_1[i].center.x, player_1[i].center.y, player_1[i].radius*2, player_1[i].radius*2);
    if (turn == 1) {
      fill(player_2[i].color_code);
      ellipse(player_2[i].center.x, player_2[i].center.y, player_2[i].radius*2, player_2[i].radius*2);
    }
  }

  vplayer2.draw();

  if (vplayer2 != null)
  {
    fill(0, 0, 0);
    text("Radius: " + p1MinCover.radius, 100, SCREENY-100);
    if (vplayer2.getCurrentRadius() < 1000) {
      colorMode(RGB, 255);
      if (vplayer2.isOverlapping())
	fill(240, 0, 0);
      else
	fill(0);
      text("Radius: " + vplayer2.getCurrentRadius(), SCREENX / 2 + 100, SCREENY-150);
      fill(0);
      text("Best Radius: " + vplayer2.getBestRadius(), SCREENX / 2 + 100, SCREENY-125);
      text("Target Radius: " + vplayer2.getTargetRadius(), SCREENX / 2 + 100, SCREENY-100);
      text("MinTarget Radius: " + vplayer2.getMinTargetRadius(), SCREENX / 2 + 100, SCREENY-75);
      text("MaxTarget Radius: " + vplayer2.getMaxTargetRadius(), SCREENX / 2 + 100, SCREENY-50);
    }
  }
  if (interval != 0) {
    delay(interval);
  }
}

void mouseReleased() {
  mouse_released = true;
  selected = -1;
}

void keyPressed() {
  switch (key) {
  case '1':  
    turn = 1;
    break;
  case '2':  
    turn = 2;
    break;
  case 'a':
  // automatic mode. find the most optimal circle.
    turn = 3;
    break;
  case 'e':
    vplayer2.endGame();
    break;
  case ' ':
    if (interval == 0) {
      interval = 100;
    } else {
        interval = 0;
    }
    break;
  }
}

