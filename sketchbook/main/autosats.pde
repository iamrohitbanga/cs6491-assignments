// Project 1 (Part A): CS 6491
// Author(s): Rohit Banga, Teresa Lee, Wing Lok, David Stolarsky
// Subject: Two Player Game to arrange circles

class AutoSatsPlayer extends VirtualPlayer {

  float delta_win = 4;
  float targetRadius;
  float minTargetRadius;
  float maxTargetRadius;
  float otherPlayerRadius;
  Circle bigCircle;
  Circle[] current;

  Circle[] bestConfiguration;
  Circle bestCover;

  PVector origin;
  float temperature = 300000;
  float alpha = 0.93;
  Circle minCover;
  int m = 0;
  int M = 75;
  float currentEnergy;
  boolean overlapFound;

  float PI = 3.142;

  int outerIter = 0;
  int maxOuterIter = 10;
  int optimizationIter = 0;
  int maxOptimizationIter = 10000;


  float getCurrentRadius() { if(minCover == null) return 999999999; return minCover.radius; }

  float getBestRadius() { if(bestCover == null) return 999999999; return bestCover.radius; }

  float getTargetRadius() { return targetRadius; }

  float getMinTargetRadius() { return minTargetRadius; }

  float getMaxTargetRadius() { return maxTargetRadius; }

  AutoSatsPlayer(Circle[] circles, PVector topLeft, PVector bottomRight) {
    super(circles, topLeft, bottomRight);

    init();
    minTargetRadius = sqrt(areaSum() / PI);
    maxTargetRadius = diameterSum() / 2;
    targetRadius = (minTargetRadius + maxTargetRadius) / 2;

    bestConfiguration = new Circle[circles.length];
    for (int i = 0; i < current.length; ++i) {
      bestConfiguration[i] = new Circle();
      bestConfiguration[i].center.set(current[i].center.x, current[i].center.y, 0);
      bestConfiguration[i].radius = current[i].radius;
      bestConfiguration[i].color_code = current[i].color_code;
    }
    bestCover = findMinimumDisk(bestConfiguration);
  }

  void step() {

    minCover = findMinimumDisk(current);
    boolean isWin = winning(current);
    if (m == M || isWin) {
      outerIter++;
      temperature = alpha * temperature;
      if (isWin || outerIter == maxOuterIter) {
	optimizationIter++;
	Circle cover = findMinimumDisk(current);
	if (!isOverlapping() && cover.radius < bestCover.radius) {
	  for (int i = 0; i < current.length; ++i) {
	    bestConfiguration[i] = new Circle();
	    bestConfiguration[i].center.set(current[i].center.x, current[i].center.y, 0);
	    bestConfiguration[i].radius = current[i].radius;
	    bestConfiguration[i].color_code = current[i].color_code;
	  }
	  bestCover = findMinimumDisk(bestConfiguration);
	  maxTargetRadius = bestCover.radius + 1;
	} else {
	  //minTargetRadius = bestCover.radius - 1;
	}
	targetRadius = (maxTargetRadius + minTargetRadius) / 2; 
	initInitialConfig();
      }
      m = 0;
    }

    if (!isOverlapping() && minCover.radius < bestCover.radius) {
      for (int i = 0; i < current.length; ++i) {
	bestConfiguration[i] = new Circle();
	bestConfiguration[i].center.set(current[i].center.x, current[i].center.y, 0);
	bestConfiguration[i].radius = current[i].radius;
	bestConfiguration[i].color_code = current[i].color_code;
      }
      bestCover = findMinimumDisk(bestConfiguration);
    }

    // move a circle
    moveRandomCircle();

    // mincover radius
    minCover = findMinimumDisk(current);
    m++;
  }

  void moveRandomCircle() {
    int indexOfCircleToMove = int(random(0, current.length));
    Circle[] trialConfig = copyConfig(current);

    Circle circleToMove = trialConfig[indexOfCircleToMove];
    float xi = circleToMove.center.x;
    float yi = circleToMove.center.y;
    float xnew = xi;
    float ynew = yi;
    for (int j = 0; j < trialConfig.length; ++j) {
      if (j == indexOfCircleToMove)
	continue;
      float xj = trialConfig[j].center.x;
      xnew += (xi-xj) * d(circleToMove, trialConfig[j]) / circleToMove.center.dist(trialConfig[j].center);
      float yj = trialConfig[j].center.y;
      ynew += (yi-yj) * d(circleToMove, trialConfig[j]) / circleToMove.center.dist(trialConfig[j].center);
    }

    xnew += -xi * d0(circleToMove) / D0(circleToMove);
    ynew += -yi * d0(circleToMove) / D0(circleToMove);

    circleToMove.center.x = xnew;
    circleToMove.center.y = ynew;

    float newEnergy = energy(trialConfig);
    float delta = newEnergy - currentEnergy;
    if (delta < 0 || random(0.0,1.0) < exp(-delta / temperature)) {
      current = trialConfig;
      currentEnergy = newEnergy;
    }
  }

  void init() {
    // translate origin to the center of the right pane
    origin = new PVector((topLeft.x  + bottomRight.x) / 2,
	(topLeft.y + bottomRight.y) / 2);

    initBigCircle();
    initInitialConfig();

    currentEnergy = energy(current);
  }

  float areaSum() {
    float area = 0;
    for (int i = 0; i < circles.length; ++i) {
      area = area + PI * circles[i].radius * circles[i].radius;
    }
    return area;
  }

  float diameterSum() {
    float diameter = 0;
    for (int i = 0; i < circles.length; ++i) {
      diameter = diameter + 2 * circles[i].radius;
    }
    return diameter;
  }

  void initInitialConfig() {
    current = new Circle[circles.length];
    for (int i = 0; i < current.length; ++i) {
      current[i] = new Circle();
      current[i].center.set(circles[i].center.x, circles[i].center.y, 0);
      // adjust center of the circle as we have translated the origin
      current[i].center.x = current[i].center.x - origin.x;
      current[i].center.y = current[i].center.y - origin.y;
      current[i].radius = circles[i].radius;
      current[i].color_code = circles[i].color_code;
    }
  }

  void initBigCircle() {
    bigCircle = new Circle();
    bigCircle.center.x = 0;
    bigCircle.center.y = 0;
    bigCircle.radius = 0;
  }

  float d(Circle a, Circle b) {
    return max(a.radius + b.radius - a.center.dist(b.center) , 0);
  }

  float d0(Circle a)
  {
    return max(a.radius - bigCircle.radius + sqrt(sq(a.center.x) + sq(a.center.y)), 0);
  }

  boolean done() {
    //return minCover.radius < otherPlayerRadius;
    return currentEnergy < 0.001;
  }

  float D0(Circle a) {
    return sqrt(sq(a.center.x) + sq(a.center.y)); 
  }

  float energy(Circle[] config) {
    float U = 0;
    float k = 1;
    for (int i = 0; i < config.length; ++i) {
      for (int j = 0; j < config.length; ++j) {
	if (i != j)
	  U += k * sq( d(config[i], config[j]) );
      }
      U += k * sq( d0(config[i]) );
    }
    return U;
  }

  Circle[] copyConfig(Circle[] src) {
    Circle[] dest = new Circle[src.length];
    for (int i = 0; i < src.length; i++) {
      dest[i] = new Circle();
      dest[i].center = new PVector();
      dest[i].center.set(src[i].center);
      dest[i].radius = src[i].radius;
      dest[i].color_code = src[i].color_code;
    }
    return dest;
  }

  void arrangeCircles(float otherPlayerRadius) {
    if (arranged == true || circles == null)
      return;

    this.otherPlayerRadius = otherPlayerRadius;
    this.targetRadius = otherPlayerRadius - delta_win;
    bigCircle.radius = this.targetRadius;
    step();
    //println(arranged);
  }

  void arrangeCirclesAutomatically() {
    if (optimizationIter == maxOptimizationIter || circles == null) {
      endGame();
      return;
    }
    bigCircle.radius = this.targetRadius;
    step();
    //println(arranged);
  }

  void draw() {

    if (bigCircle == null || minCover == null)
      return;

    translate(origin.x, origin.y);
    noFill();
    colorMode(RGB, 255);
    stroke(240, 0, 0);
    ellipse(bigCircle.center.x, bigCircle.center.y, bigCircle.radius * 2, bigCircle.radius * 2);
    stroke(0);
    ellipse(minCover.center.x, minCover.center.y, minCover.radius * 2, minCover.radius * 2);
    stroke(0, 100, 0);
    ellipse(bestCover.center.x, bestCover.center.y, bestCover.radius * 2, bestCover.radius * 2);
    stroke(0);
    for (int i = 0; i < current.length; ++i) {
      colorMode(HSB, 255);
      fill(current[i].color_code);
      ellipse(current[i].center.x, current[i].center.y, current[i].radius*2, current[i].radius*2);
    }
    translate(-origin.x, -origin.y);
  }

  boolean winning(Circle[] config) {
    overlapFound = false;
    for (int i = 0; i < config.length; i++) {
      for (int j = 0; j < config.length; j++) {
	if (i == j)
	  continue;
	if (overlap(config[i], config[j])) {
	  overlapFound = true;
	  return false;
	}
      }
    }

    if (minCover.radius < targetRadius || minCover.radius < bestCover.radius)
      return true;

    return false;
  }

  boolean overlap(Circle c1, Circle c2) {
    return c1.center.dist(c2.center) <= (c1.radius + c2.radius-1);
  }

  boolean isOverlapping() {
    return overlapFound;
  }

  void endGame() {
    optimizationIter = maxOptimizationIter;
    overlapFound = false;
    current = bestConfiguration;
    minCover = bestCover;
    bigCircle = bestCover;
  }
}
