// Project 1 (Part A): CS 6491
// Author(s): Rohit Banga, Teresa Lee, Wing Lok, David Stolarsky
// Subject: Two Player Game to arrange circles

abstract class VirtualPlayer {

  Circle[] circles;
  PVector topLeft;
  PVector bottomRight;
  boolean arranged;

  VirtualPlayer(Circle[] circles, PVector topLeft, PVector bottomRight) {
    this.circles = circles;
    this.topLeft = topLeft;
    this.bottomRight = bottomRight;
    this.arranged = false;
  }

  abstract void arrangeCircles(float targetRadius);
  abstract void arrangeCirclesAutomatically();
  abstract float getCurrentRadius();
  abstract float getBestRadius();
  abstract float getTargetRadius();
  abstract float getMinTargetRadius();
  abstract float getMaxTargetRadius();
  abstract void draw();
  abstract boolean isOverlapping();
  abstract void endGame();
}

class VirtualPlayerExample extends VirtualPlayer {

  VirtualPlayerExample(Circle[] circles, PVector topLeft, PVector bottomRight) {
    super(circles, topLeft, bottomRight);
  }

  float getCurrentRadius() {
    return 0.0;
  }

  float getBestRadius() {
    return 0.0;
  }

  float getTargetRadius() {
    return 0.0;
  }

  float getMinTargetRadius() {
    return 0.0;
  }

  float getMaxTargetRadius() {
    return 0.0;
  }

  void arrangeCircles(float targetRadius) {
    if (arranged == true)
      return;
    if (circles != null) {
      for (Circle c : circles) {
	c.center.x = (topLeft.x + bottomRight.x) / 2;
	c.center.y = (bottomRight.y - c.center.y);
      }
    }
    arranged = true;
  }

  void draw() {
  }

  boolean isOverlapping() {
    return false;
  }

  void arrangeCirclesAutomatically() {
  }

  void endGame() {
  }
}
