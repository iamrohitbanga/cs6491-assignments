// Project 1 (Part A): CS 6491
// Author(s): Rohit Banga, Teresa Lee, Wing Lok, David Stolarsky
// Subject: Two Player Game to arrange circles

class Circle {
  public PVector center;
  public float radius;
  public color color_code;

	Circle() {
		center = new PVector();
		radius = 10;
		color_code = 0;
	}
}
