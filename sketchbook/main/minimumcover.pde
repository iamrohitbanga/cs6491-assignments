// Project 1 (Part A): CS 6491
// Author(s): Rohit Banga, Teresa Lee, Wing Lok, David Stolarsky
// Subject: Two Player Game to arrange circles

Circle findMinimumDisk(Circle[] _disks)
{
  //println("----------------starting search---------------");

  List<disk> diskList = new ArrayList<disk>();
  for(Circle _d : _disks)
  {
    disk d = new disk(_d.center.x, _d.center.y, _d.radius);
    diskList.add(d);
  }
  disk[] disks = diskList.toArray(new disk[0]);

  int diskCount = disks.length;

  // construct all covering disks determined by 2 and 3 tuples of disks
  int diskCountChoose3 = diskCount * (diskCount - 1) * (diskCount - 2)/ 6; 
  int diskCountChoose2 = diskCount * (diskCount - 1) / 2; 
  SpanningDiskCombo[] combos = new SpanningDiskCombo[diskCountChoose3 + diskCountChoose2]; // (diskCount choose 3) + (diskCount choose 2)
  int p = 0;
  //println("3 ways:");
  for(int d1 = 0; d1 < diskCount; d1++)
  {
    for(int d2 = d1 + 1; d2 < diskCount; d2++)
    {
      for(int d3 = d2 + 1; d3 < diskCount; d3++)
      {
        combos[p] = new SpanningDiskCombo(disks[d1], disks[d2], disks[d3]);
        p++;
      }
    }
  }
  //println("2 ways:");
  for(int d1 = 0; d1 < diskCount; d1++)
  {
    for(int d2 = d1 + 1; d2 < diskCount; d2++)
    {
      combos[p] = new SpanningDiskCombo(disks[d1], disks[d2]);
      p++;
    }
  }

  // debug draw the 3's
  List<disk> candidates = new ArrayList<disk>();
  for(SpanningDiskCombo combo : combos)
  {
    if(combo != null && combo.coveringDisk != null && combo.d3 != null)
    {
      candidates.add(combo.coveringDisk);
    }
  }
  //debugDisks = candidates;
  //println("There are " + debugDisks.size() + " 3-disk cover candidates");

  //println(combos.length + " combos");

  Arrays.sort(combos);
  for(SpanningDiskCombo C : combos)
  {
    if(C != null && C.coveringDisk != null && C.coveringDisk.contains(disks))
    {
      //println( (C.d3 == null ? "I'm a 2" : "I'm a 3") + " -- radius: " + C.coveringDisk.r);
      
      Circle minCover = new Circle();
      minCover.center = new PVector(C.coveringDisk.x, C.coveringDisk.y);
      minCover.radius = C.coveringDisk.r;
      return minCover;
    }
  }

  println("Uh oh, findMinimumDisk did not find anything! it should have found something.");
  return null;
}


class Point
{
  public Point(float _x, float _y) { x = _x; y = _y; }
  float x, y;
  public float distanceTo(Point other) { return sqrt(sq(x-other.x)+sq(y-other.y)); }
}

class disk
{
  float x=0, y=0, r=10;
  color c = color(200, 200, 200);
  boolean highlight = false;
  
  disk(float px, float py, float pr) {r=pr; x=px; y=py;}
  Point center() { return new Point(x, y); }
  disk set_radius_to_mouse() {r=sqrt(sq(x-mouseX)+sq(y-mouseY)); return this;}
  disk set_center_to_mouse() {x=mouseX; y=mouseY; return this;}
  disk show()
  {
    strokeWeight(3);
    stroke(255, 0, 0);
    if(highlight)
      fill(c);
    else
      fill(c);
    ellipse(x, y, 2*r, 2*r);
    return this;
  }
  void debugShow()
  {
    strokeWeight(1);
    stroke(0);
    noFill();
    ellipse(x, y, 2*r, 2*r);
  }

  float dis_ctr_to_mouse() {return sqrt(sq(x-mouseX)+sq(y-mouseY));}
  boolean covers_point(float _x, float _y) { return sqrt(sq(_x-x)+sq(_y-y)) < r; }
  boolean overlaps(disk other) { return borderDistanceTo(other) < 0.0; }
  float dis_border_to_mouse() {return abs(dis_ctr_to_mouse()-r);}
  float borderDistanceTo(disk other) { return sqrt(sq(x-other.x)+sq(y-other.y)) - r - other.r; }
  float centerDistanceTo(disk other) { return sqrt(sq(x-other.x)+sq(y-other.y)); }
  public float farBorderDistanceTo(disk other) { return centerDistanceTo(other) + r + other.r; }
  public boolean contains(Point p) { return sqrt(sq(x-p.x)+sq(y-p.y)) <= r; }
  public boolean containsCenterOf(disk d) { return contains(d.center()); }
  public boolean contains(disk other)
  {
    if( centerDistanceTo(other) + other.r - r > EPSILON )
      return false;
    else
      return true;
  }
  public boolean containsCentersOf(disk[] _disks)
  {
    for(disk d : _disks)
    {
      if(!containsCenterOf(d))
        return false;
    }
    return true;
  }
  public boolean contains(disk[] _disks)
  {
    for(disk d : _disks)
    {
      if(!contains(d))
        return false;
    }
    return true;
  }
}

class SpanningDiskCombo implements Comparable
{
  disk d1 = null, d2 = null, d3 = null;
 
  disk coveringDisk = null;

  public int compareTo(Object o)
  {
    SpanningDiskCombo other = (SpanningDiskCombo) o;

    if(coveringDisk == null && other.coveringDisk == null)
      return 0;
    else if(coveringDisk == null)
      return 1;
    else if (other.coveringDisk == null)
      return -1;

    if(coveringDisk.r < other.coveringDisk.r)
      return -1;
    else if(coveringDisk.r == other.coveringDisk.r)
      return 0;
    else
      return 1;
  }

  public SpanningDiskCombo(disk _d1, disk _d2)
  {
    d1 = _d1; d2 = _d2;

    coveringDisk = minimumCoveringDisk();
    
    //if(coveringDisk != null)
      //println("2 way disk: " + coveringDisk.x + ", " + coveringDisk.y + " -- " + coveringDisk.r);
  }
 
  public SpanningDiskCombo(disk _d1, disk _d2, disk _d3)
  {
    d1 = _d1; d2 = _d2; d3 = _d3;

    coveringDisk = minimumCoveringDisk();
    
    //f(coveringDisk != null)
      //println("3 way disk: " + coveringDisk.x + ", " + coveringDisk.y + " -- " + coveringDisk.r);
  }

  private disk minimumCoveringDisk()
  {
    //disk coveringDisk = null;

    // 2 disk case
    if(d3 == null)
    {
      disk candidate = new disk(0, 0, 0);
      float centerDist = d1.centerDistanceTo(d2);
      float centerDistX = d2.x - d1.x;
      float centerDistY = d2.y - d1.y;
      float coveringSegmentX1 = d1.x - d1.r * centerDistX / centerDist;
      float coveringSegmentY1 = d1.y - d1.r * centerDistY / centerDist;
      float coveringSegmentX2 = d2.x + d2.r * centerDistX / centerDist;
      float coveringSegmentY2 = d2.y + d2.r * centerDistY / centerDist;
      float coveringSegmentLength = sqrt(sq(coveringSegmentX1 - coveringSegmentX2)+sq(coveringSegmentY1 - coveringSegmentY2));
      candidate.x = (coveringSegmentX1 + coveringSegmentX2) / 2.0;
      candidate.y = (coveringSegmentY1 + coveringSegmentY2) / 2.0;

      candidate.r = max( candidate.centerDistanceTo(d1) + d1.r,
                         candidate.centerDistanceTo(d2) + d2.r );

      //if(candidate.contains(disks))
      //  coveringDisk = candidate;


      return candidate;
    }

    // 3 disk case
    ACircle c1 = new ACircle(new double[]{d1.x, d1.y}, d1.r);
    ACircle c2 = new ACircle(new double[]{d2.x, d2.y}, d2.r);
    ACircle c3 = new ACircle(new double[]{d3.x, d3.y}, d3.r);

    ApolloniusSolver solver = new ApolloniusSolver();
    ACircle solution = solver.solveApollonius(c1, c2, c3, 1, 1, 1);
    disk solutionDisk = new disk((float)solution.center[0], (float)solution.center[1], abs((float)solution.radius));


    //println("--d3--- " + d3.x + ", " + d3.y + ", " + d3.r);
    //println("---solution-- " + solutionDisk.x + ", " + solutionDisk.y + ", " + solutionDisk.r);

    //if(solutionDisk.contains(d1.center()) && solutionDisk.contains(d2.center()) && solutionDisk.contains(d3.center()))
    //if(solutionDisk.contains(disks))
      return solutionDisk;
    //else
      //return null;

  }
}

/*********** holy crap solution from Rosetta Code http://rosettacode.org/wiki/Problem_of_Apollonius#Java *************/

public class ACircle
{
 public double[] center;
 public double radius;
 public ACircle(double[] center, double radius)
 {
  this.center = center;
  this.radius = radius;
 }
 public String toString()
 {
  return String.format("Circle[x=%.2f,y=%.2f,r=%.2f]",center[0],center[1],
		       radius);
 }
}
 
public class ApolloniusSolver
{
/** Solves the Problem of Apollonius (finding a circle tangent to three other
  * circles in the plane). The method uses approximately 68 heavy operations
  * (multiplication, division, square-roots). 
  * @param c1 One of the circles in the problem
  * @param c2 One of the circles in the problem
  * @param c3 One of the circles in the problem
  * @param s1 An indication if the solution should be externally or internally
  *           tangent (+1/-1) to c1
  * @param s2 An indication if the solution should be externally or internally
  *           tangent (+1/-1) to c2
  * @param s3 An indication if the solution should be externally or internally
  *           tangent (+1/-1) to c3
  * @return The circle that is tangent to c1, c2 and c3. 
  */
 public ACircle solveApollonius(ACircle c1, ACircle c2, ACircle c3, int s1,
				      int s2, int s3)
 {
  double x1 = c1.center[0];
  double y1 = c1.center[1];
  double r1 = c1.radius;
  double x2 = c2.center[0];
  double y2 = c2.center[1];
  double r2 = c2.radius;
  double x3 = c3.center[0];
  double y3 = c3.center[1];
  double r3 = c3.radius;
 
  //Currently optimized for fewest multiplications. Should be optimized for
  //readability
  double v11 = 2*x2 - 2*x1;
  double v12 = 2*y2 - 2*y1;
  double v13 = x1*x1 - x2*x2 + y1*y1 - y2*y2 - r1*r1 + r2*r2;
  double v14 = 2*s2*r2 - 2*s1*r1;
 
  double v21 = 2*x3 - 2*x2;
  double v22 = 2*y3 - 2*y2;
  double v23 = x2*x2 - x3*x3 + y2*y2 - y3*y3 - r2*r2 + r3*r3;
  double v24 = 2*s3*r3 - 2*s2*r2;
 
  double w12 = v12/v11;
  double w13 = v13/v11;
  double w14 = v14/v11;
 
  double w22 = v22/v21-w12;
  double w23 = v23/v21-w13;
  double w24 = v24/v21-w14;
 
  double P = -w23/w22;
  double Q = w24/w22;
  double M = -w12*P-w13;
  double N = w14 - w12*Q;
 
  double a = N*N + Q*Q - 1;
  double b = 2*M*N - 2*N*x1 + 2*P*Q - 2*Q*y1 + 2*s1*r1;
  double c = x1*x1 + M*M - 2*M*x1 + P*P + y1*y1 - 2*P*y1 - r1*r1;
 
  // Find a root of a quadratic equation. This requires the circle centers not
  // to be e.g. colinear
  double D = b*b-4*a*c;
  double rs = (-b-Math.sqrt(D))/(2*a);
  double xs = M + N * rs;
  double ys = P + Q * rs;
  return new ACircle(new double[]{xs,ys}, rs);
 }


}
