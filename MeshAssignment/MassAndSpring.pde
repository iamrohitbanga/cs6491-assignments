class Pair implements Comparable<Pair> {
  int x;
  int y;
  public Pair() { x=0; y=0; }
  public Pair(int x, int y) {
    this.x = x;
    this.y = y;
  }
  public int compareTo(Pair p) {
    if (this.x < p.x)
      return -1;
    if (this.x == p.x) {
      if ((this.y - p.y) < 0)
        return -1;
      else if (this.y == p.y)
        return 0;
      else
        return 1;
    }
    return 1;
  }
}

class MeshPhysicsProfile {

  Mesh mesh;
  boolean use_default_spring_constant = true;
  float youngs_modulus = 50;
  float DEFAULT_MASS = 70;
  float DEFAULT_SPRING_CONSTANT = 30;
  float gravity = 10;
  float DEFAULT_DAMPING_CONSTANT = 30;
  List<Integer> pinVertices;
  float[][] originalLengths;

  Map<Pair, Integer> adjTriangle1;
  Map<Pair, Integer> adjTriangle2;
  PVector gravityUnitVector = new PVector(0, 1, 0);
  PVector[] velocities;
  PVector[] force;

  MeshPhysicsProfile(Mesh mesh) {
    this.mesh = mesh;
    this.velocities = new PVector[mesh.getNumVertices()];
    disturb();
    initPinVertices();
    initOriginalLengths();
    initAdjTriangles();
    force = new PVector[mesh.getNumVertices()];
  }

  void replaceMesh(Mesh mesh) {
    this.mesh = mesh;
    PVector[] newVelocities = new PVector[mesh.getNumVertices()];
    for (int i = 0; i < this.velocities.length; ++i) {
      newVelocities[i] = this.velocities[i];
    }
    for (int i = 0; i < newVelocities.length; ++i) {
      if (newVelocities[i] == null)
        newVelocities[i] = new PVector();
    }
    this.velocities = newVelocities;
    initOriginalLengths();
    initAdjTriangles();
    force = new PVector[mesh.getNumVertices()];
  }

  void disturb() {
    for (int i = 0; i < velocities.length; ++i) {
      if (velocities[i] == null)
        velocities[i] = new PVector(random(30), random(30), 0);
      else
        velocities[i].add(new PVector(random(30), random(30), 0));
    }
  }

  float getMass(int vertex_id) {
    return 500 / mesh.getNumVertices();
  }

  float getSpringConstant(int v1, int v2) {
    if (use_default_spring_constant)
      return DEFAULT_SPRING_CONSTANT;
    Integer t1 = adjTriangle1.get(new Pair(v1, v2));
    Integer t2 = adjTriangle2.get(new Pair(v2, v1));
    float area_sum = 0;

    assert (t1 != null);
    area_sum = triangleArea(mesh.getVertexFromCorner(t1*3),
                              mesh.getVertexFromCorner(t1*3 + 1),
                              mesh.getVertexFromCorner(t1*3 + 2));
    if (t2 != null) {
      area_sum += triangleArea(mesh.getVertexFromCorner(t2*3),
                              mesh.getVertexFromCorner(t2*3 + 1),
                              mesh.getVertexFromCorner(t2*3 + 2));
    }
    assert (area_sum != 0);
    assert (originalLengths[v1][v2] != 0);
    float spring_constant = youngs_modulus * area_sum / sq(originalLengths[v1][v2]);
    return spring_constant;
  }

  float triangleArea(PVector p1, PVector p2, PVector p3) {
    float a = p1.dist(p2);
    float b = p2.dist(p3);
    float c = p3.dist(p1);
    float s = (a + b + c) / 2;
    // using Hero's formula
    return sqrt( s * (s-a) * (s-b) * (s-c));
  }

  float getDampingConstant(int vertex_id) {
    return DEFAULT_DAMPING_CONSTANT;
  }

  void initAdjTriangles() {
     adjTriangle1 = new TreeMap<Pair, Integer>();
     adjTriangle2 = new TreeMap<Pair, Integer>();
     for (int i = 0; i < mesh.getNumTriangles(); ++i) {
      int v[] = mesh.getTriangleVertices(i);
      Pair[] edges = new Pair[6];
      edges[0] = new Pair(v[0], v[1]); 
      edges[1] = new Pair(v[1], v[0]); 
      edges[2] = new Pair(v[1], v[2]); 
      edges[3] = new Pair(v[2], v[1]); 
      edges[4] = new Pair(v[2], v[0]); 
      edges[5] = new Pair(v[0], v[2]);
      for (int j = 0; j < edges.length; ++j) {
        if (adjTriangle1.get(edges[j]) == null) {
          adjTriangle1.put(edges[j],i);
        }
        else {
          adjTriangle2.put(edges[j], i);
        }
      }
     }
  }

  void initOriginalLengths() {
    originalLengths = new float[mesh.getNumVertices()][];
    for (int i = 0; i < originalLengths.length; ++i) {
      originalLengths[i] = new float[mesh.getNumVertices()];
    }
    for (int i = 0; i < mesh.getNumTriangles(); ++i) {
      int vertices[] = mesh.getTriangleVertices(i);
      assert (vertices != null && vertices.length == 3);
      PVector v1 = mesh.getVertex(vertices[0]);
      PVector v2 = mesh.getVertex(vertices[1]);
      PVector v3 = mesh.getVertex(vertices[2]);

      originalLengths[vertices[0]][vertices[1]] = v1.dist(v2);
      originalLengths[vertices[1]][vertices[0]] = v2.dist(v1);

      originalLengths[vertices[1]][vertices[2]] = v2.dist(v3);
      originalLengths[vertices[2]][vertices[1]] = v3.dist(v2);

      originalLengths[vertices[2]][vertices[0]] = v3.dist(v1);
      originalLengths[vertices[2]][vertices[0]] = v1.dist(v3);
    }
  }

  void initPinVertices() {
    int npin = 2;
    pinVertices = new ArrayList<Integer>();
    if (mesh.getVertex(0).y < mesh.getVertex(1).y) {
      pinVertices.add(0);
      pinVertices.add(1);
    } else {
      pinVertices.add(1);
      pinVertices.add(0);
    }

    for (int i = 2; i < mesh.getNumVertices(); i++) {
      int v1Index = pinVertices.get(0);
      int v2Index = pinVertices.get(1);
      PVector v1 = mesh.getVertex(v1Index);
      PVector v2 = mesh.getVertex(v2Index);
      PVector v3 = mesh.getVertex(i);
      if (v3.y < v1.y) {
        pinVertices.set(1, v2Index);
        pinVertices.set(0, i);
      }
      else if (v3.y < v2.y) {
        pinVertices.set(1, i);
      }
    }
  }

  void draw() {
    if (pinVertices != null) {
      stroke(255);
      for (int i = 0; i < pinVertices.size(); ++i) {
        line(mesh.getVertex(pinVertices.get(i)).x, 0, mesh.getVertex(pinVertices.get(i)).x, mesh.getVertex(pinVertices.get(i)).y);
      }
    }
//    try {
//      Thread.sleep(50);
//    }
//    catch (Exception ex) {
//      System.exit(0);
//    }
  }

  // steps the physical simulation of the mesh's mass and spring system
  // by timeDelta seconds.
  // modifies points in mesh.G[] to reflect the mesh's new state.
  void stepSimulation(int timeDeltaMilliseconds)
  {
    if (pinVertices == null)
      return;
    if (mesh == null)
      return;

    // let's get physical, physical.
    println("Stepping physics forward by " + timeDeltaMilliseconds + " milliseconds.");

    int ntriangles = mesh.getNumTriangles();
    for (int i = 0; i < mesh.getNumVertices(); ++i) {
      force[i] = new PVector();
      force[i].set(gravityUnitVector);
      force[i].mult(getMass(i) * gravity);
    }

    for (int i = 0; i < ntriangles; ++i) {
       int[] vertices = mesh.getTriangleVertices(i);
       assert(vertices != null && vertices.length == 3);
       PVector v0 = mesh.getVertex(vertices[0]);
       PVector v1 = mesh.getVertex(vertices[1]);
       PVector v2 = mesh.getVertex(vertices[2]);
       float k01 = getSpringConstant(vertices[0], vertices[1]);
       float k12 = getSpringConstant(vertices[1], vertices[0]);
       float k20 = getSpringConstant(vertices[2], vertices[0]);

       PVector force01 = new PVector();
       force01.set(v1);
       force01.sub(v0);
       force01.mult(1 / force01.mag());
       PVector force12 = new PVector();
       force12.set(v2);
       force12.sub(v1);
       force12.mult(1 / force12.mag());
       PVector force20 = new PVector();
       force20.set(v0);
       force20.sub(v2);
       force20.mult(1 / force20.mag());

       float x01 = v0.dist(v1) - originalLengths[vertices[0]][vertices[1]];
       force01.mult(k01 * x01);
       force[vertices[0]].add(force01);
       force01.mult(-1);
       force[vertices[1]].add(force01);

       float x12 = v1.dist(v2) - originalLengths[vertices[1]][vertices[2]];
       force12.mult(k12 * x12);
       force[vertices[1]].add(force12);
       force12.mult(-1);
       force[vertices[2]].add(force12);

       float x20 = v2.dist(v0) - originalLengths[vertices[2]][vertices[0]];
       force20.mult(k20 * x20);
       force[vertices[2]].add(force20);
       force20.mult(-1);
       force[vertices[0]].add(force20);
     }

     float delta_t = timeDeltaMilliseconds * 0.01/2;
     for (int i = 0; i < mesh.getNumVertices(); ++i) {
       if (pinVertices.contains(i))
         continue;
       PVector damping_force = new PVector();
       damping_force.set(velocities[i]);
       damping_force.mult(-getDampingConstant(i));
       force[i].add(damping_force);
       force[i].mult(1.0 / getMass(i));
       PVector p = mesh.getVertex(i);
       float delta_x = velocities[i].x * delta_t + 0.5 * force[i].x * sq(delta_t);
       float delta_y = velocities[i].y * delta_t + 0.5 * force[i].y * sq(delta_t);
       mesh.setVertex(i, new PVector(p.x+delta_x, p.y+delta_y, 0));
       force[i].mult(delta_t);
       velocities[i].add(force[i]);
     }
    println("done");
  }

  void togglePinVertex(int vertex_id) {
    if (pinVertices.contains(vertex_id)) {
      pinVertices.remove(((Object)vertex_id));
    }
    else {
      pinVertices.add(vertex_id);
      PVector v = mesh.getVertex(vertex_id);
      v.y = v.y / 2;
      mesh.setVertex(vertex_id, v);
    }
  }

}

