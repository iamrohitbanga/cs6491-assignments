class Triangle
{
  Point p1, p2, p3;

  public Triangle(Point p1, Point p2, Point p3)
  {
    this.p1 = p1;
    this.p2 = p2;
    this.p3 = p3;
  }

  void draw() {
    triangle(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
  }

  boolean contains(Point p)
  {
    Vector p1p2 = new Vector(p1, p2);
    Vector p1p3 = new Vector(p1, p3);
    Vector p2p3 = new Vector(p2, p3);
    Vector p2p1 = new Vector(p2, p1);
    Vector p3p1 = new Vector(p3, p1);
    Vector p3p2 = new Vector(p3, p2);

    Vector p1p = new Vector(p1, p);
    Vector p2p = new Vector(p2, p);
    Vector p3p = new Vector(p3, p);

    if(
      crossParity(p1p2, p1p3) == crossParity(p1p2, p1p) &&
      crossParity(p2p3, p2p1) == crossParity(p2p3, p2p) &&
      crossParity(p3p1, p3p2) == crossParity(p3p1, p3p)
    )
      return true;
    else
      return false;
  }

  boolean containsAnyOf(List<Point> points)
  {
    for(Point p : points)
    {
      if(contains(p))
        return true;
    }
    return false;
  }
}

