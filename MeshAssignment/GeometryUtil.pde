float PI = 3.14159265359;

/***************** Point stuff ****************/
class Point
{
  float x, y;
  public Point() {x = 0; y = 0;};
  public Point(float _x, float _y) { x = _x; y = _y; }
  public Point(PVector vec) { this.x = vec.x; this.y = vec.y; }
  Vector toVector() { return new Vector(this.x, this.y); }
  PVector toPVector() { return new PVector(this.x, this.y); }
  Point plus(Vector v) { return new Point(this.x + v.x, this.y + v.y); }
  public float distanceTo(Point other) { return sqrt(sq(x-other.x)+sq(y-other.y)); }
  /*boolean equals(Object o)
  {
    return this == (Point) o;
  }
  int hashCode()
  {
    return int(this.x) + int(this.y);
  }*/
}

Point average(Point[] points)
{
  Vector runningAvg = new Vector(0.0, 0.0);
  for(int p = 0; p < points.length; p++)
  {
    float runningAvgWeight = float(p) / float(p+1);
    float newPointWeight = 1.0 - runningAvgWeight;

    runningAvg = runningAvg.scaled(runningAvgWeight).plus(points[p].toVector().scaled(newPointWeight));
  }
  return new Point(0.0, 0.0).plus(runningAvg);
}

class DistanceTo implements Comparator
{
  Point pointOfInterest;
  
  public DistanceTo(Point p)
  {
    pointOfInterest = p;
  }
  
  public int compare(Object o1, Object o2)
  {
    Point p1 = (Point) o1;
    Point p2 = (Point) o2;
    
    float distanceToP1 = pointOfInterest.distanceTo(p1);
    float distanceToP2 = pointOfInterest.distanceTo(p2);
    
    if(distanceToP1 < distanceToP2)
      return -1;
    else if(distanceToP1 == distanceToP2)
      return 0;
    else
      return 1;
  }
}

Point[] nearestNeighborArray(List<Point> points, Point p)
{
  Point[] pointsByDistToP = points.toArray(new Point[0]);
  Arrays.sort(pointsByDistToP, new DistanceTo(p));
  return pointsByDistToP;
}

Point nearestNeighbor(List<Point> points, Point p)
{
  Point[] pointsByDistToP = nearestNeighborArray(points, p);
  return pointsByDistToP[0];
}

Point leftMostPoint(ArrayList<Point> points)
{
  Point leftMost = points.get(0);
  for(Point p : points)
  {
    if(p.x < leftMost.x)
      leftMost = p;
  }
  return leftMost;
}

Point circumcenter(Point A, Point B, Point C)
{
  float D = 2*(A.x*(B.y-C.y) + B.x*(C.y-A.y) + C.x*(A.y-B.y));

  float x = ((sq(A.x)+sq(A.y))*(B.y-C.y) + (sq(B.x)+sq(B.y))*(C.y-A.y) + (sq(C.x)+sq(C.y))*(A.y-B.y))/D;
  float y = ((sq(A.x)+sq(A.y))*(C.x-B.x) + (sq(B.x)+sq(B.y))*(A.x-C.x) + (sq(C.x)+sq(C.y))*(B.x-A.x))/D;

  return new Point(x, y);
}

/*********************** Segment stuff ************************/

class Segment
{
  Point p1;
  Point p2;

  Segment(Point p1, Point p2) { this.p1 = p1; this.p2 = p2; }

  float length() { return p1.distanceTo(p2); }
  
  Point midpoint()
  {
    float x = 0.5 * p1.x + 0.5 * p2.x;
    float y = 0.5 * p1.y + 0.5 * p2.y;
    return new Point(x, y);
  }

  float bulgeOf(Point p)
  {
    Point bulgeCircleCenter = circumcenter(this.p1, this.p2, p);
    float bulgeCircleRadius = bulgeCircleCenter.distanceTo(p);
    Point bulgeStart = this.midpoint();
    Point bulgeEnd;
    

    Vector p1p2 = new Vector(p1, p2);
    float angleToCandidate = p1p2.angleTo(new Vector(p1, p));
    float angleToBulgeCenter = p1p2.angleTo(new Vector(p1, bulgeCircleCenter));

    float bulge;
    if( (angleToCandidate > 0) == (angleToBulgeCenter > 0) )
      bulge = bulgeStart.distanceTo(bulgeCircleCenter) + bulgeCircleRadius;
    else
      bulge = bulgeCircleRadius - bulgeStart.distanceTo(bulgeCircleCenter);

    return bulge;
  }

  Point findMinBulgePoint(ArrayList<Point> points)
  {
    assert(points.size() > 0);

    if(points.size() == 1)
      return points.get(0);

    float minBulge = this.bulgeOf(points.get(0));
    Point minBulgePoint = points.get(0);
    for(Point p : points)
    {
      float pBulge = this.bulgeOf(p);
      if(pBulge < minBulge)
      {
        minBulge = pBulge;
	minBulgePoint = p;
      }
    }
    return minBulgePoint;
  }
}

class Halfspace
{
  Segment s; // p in halfspace if 0 <= theta <= PI
             // theta = angle from Vector(s.p1--->s.p2) to Vector(s.p1--->p)

  Halfspace(Segment s)
  {
    this.s = s;
  }

  Vector halfspaceVector() { return new Vector(s.p1, s.p2); }

  boolean contains(Point p)
  {
    float angleToPoint = this.halfspaceVector().angleTo(new Vector(s.p1, p));
    return angleToPoint <= 0 && angleToPoint >= - PI;
  }

  ArrayList<Point> intersection(ArrayList<Point> points)
  {
    ArrayList<Point> intersection = new ArrayList<Point>();

    for(Point p : points)
    {
      if(this.contains(p))
        intersection.add(p);
    }

    return intersection;
  }
}

// This "right halfspace" looks like a "left halfspace" in Processing's default coordinates, because y+ axis points down. 
// Using "left" to mean positive theta (left from origin, facing x+ axis direction, as in the unit circle)
class RightHalfspaceSegment extends Segment
{
  RightHalfspaceSegment(Point p1, Point p2)
  {
    super(p1, p2);
  }

  void draw()
  {
    line(this.p1.x, this.p1.y, this.p2.x, this.p2.y);
    
    Point arrowStart = this.midpoint();
    Point arrowEnd = arrowStart.plus(this.direction());
    line(arrowStart.x, arrowStart.y, arrowEnd.x, arrowEnd.y);
  }

  Vector direction()
  {
    return new Vector(p1, p2).rotated(- PI / 2.0).normalized().scaled(40);
  }

  Halfspace halfspace()
  {
    return new Halfspace(this);
  }

  boolean equals(Object o)
  {
    RightHalfspaceSegment other = (RightHalfspaceSegment) o;
    return this.p1 == other.p1 && this.p2 == other.p2;
  }

  int hashCode()
  {
    return int(this.p1.x) +
           int(this.p1.y) +
           int(this.p2.x) +
           int(this.p2.y);
  }
}

