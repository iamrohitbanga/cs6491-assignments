
float CUT_VERTEX_DISTANCE = 50.0;
int NUMBER_OF_RANDOM_POINTS = 10;

boolean doPhysics = true;
int lastTime;

Triangulator triangulator = new Triangulator();
boolean illustrateCircumcenterAndBulge = false;

Mesh mesh = makeTestMesh();
boolean debugDrawMesh = false;
MeshPhysicsProfile physics;


PImage knife;
float knifeScale = 0.35;
Vector knifeDirection = null;
float knifeDirectionLowPassPower = 0.8;

int SCREENX = 1024;
int SCREENY = 650;

void setup() 
{
  size(SCREENX, SCREENY, P3D); 
  noStroke();
  rectMode(CENTER);
  initPhysics();
  knife = loadImage("knife--tip_at_234_1022.png");
  lastTime = millis();
}

void initPhysics() {
  if(doPhysics)
    physics = new MeshPhysicsProfile(mesh);
}

void reinitPhysics() {
  if (physics == null)
    initPhysics();
  else
    physics.replaceMesh(mesh);
}

void draw() 
{
  background(0); 
  // physics
  int newTime = millis();
  int timeDelta = newTime - lastTime;
  if(doPhysics)
    physics.stepSimulation(timeDelta);
  lastTime = newTime;

  // mesh
  stroke(0);
  fill(100, 200, 150);
  strokeWeight(1);
  mesh.draw();
  if(debugDrawMesh)
    mesh.debugDraw();
  
  // triangulation UI
  triangulator.draw();
  
  // cut UI
  if(cutPolyline.size() > 0)
  {
    stroke(250, 177, 7);
    strokeWeight(3);
    PVector previous = cutPolyline.get(0);
    for(PVector p : cutPolyline)
    {
      line(previous.x, previous.y, p.x, p.y);
      previous = p;
    }
    strokeWeight(6);
    stroke(250, 10, 10);
    for(PVector p : cutPolyline)
    {
      point(p.x, p.y);
    }
    
    Vector cutDirection = new Vector(previous.x, previous.y, mouseX, mouseY);
    if(knifeDirection == null)
      knifeDirection = cutDirection;
    else
      knifeDirection = knifeDirection.scaled(knifeDirectionLowPassPower).plus(cutDirection.scaled(1.0 - knifeDirectionLowPassPower));
    pushMatrix();
      translate(mouseX, mouseY);
      scale(knifeScale);
      rotateZ(knifeDirection.angle());
      translate(-234, -1022);
      image(knife, 0, 0);
    popMatrix();
  }

  if (doPhysics)
    physics.draw();
  fill(255, 255, 255);
  text("(" + mouseX + "," + mouseY + ")", 0, SCREENY-100);
}

void mouseClicked()
{
  if (mouseButton == LEFT)
    triangulator.addPoint(mouseX, mouseY);
  else {
    int vertex_id = 0;
    for (int i = 0; i < mesh.getNumVertices(); ++i) {
      PVector mousePos = new PVector(mouseX, mouseY, 0);
      if (doPhysics && mousePos.dist(mesh.getVertex(i)) < 20) {
        doPhysics = false;
        physics.togglePinVertex(i);
        doPhysics = true;
      }
    }
  }
}

boolean cutting = false;
PVector cutPreviousPoint;
Vertex previousCutEndVertex = null;
ArrayList<PVector> cutPolyline = new ArrayList<PVector>();
void mouseDragged()
{
  PVector mouse = new PVector(mouseX, mouseY);
  if(cutting == false)
  {
    cutting = true;
    cutPreviousPoint = mouse;
    cutPolyline.add(mouse);
    knifeDirection = null;
  }
  else
  {
    if(cutPreviousPoint.dist(mouse) >= CUT_VERTEX_DISTANCE)
    {
      if(previousCutEndVertex == null)
      {
        doPhysics = false;
        previousCutEndVertex = cutFromPointToPoint(mesh, cutPreviousPoint, mouse);
        reinitPhysics();
        doPhysics = true;
        //println("doing cut segment " + cutPolyline.size() + ": cutFromPointToPoint(mesh, " + cutPreviousPoint + ", " + mouse + ")");
      }
      else
      {
        doPhysics = false;
        previousCutEndVertex = cutFromVertexToPoint(mesh, previousCutEndVertex, mouse);
        reinitPhysics();
        doPhysics = true;
        //println("doing cut segment " + cutPolyline.size() + ": cutFromVertexToPoint(mesh, " + previousCutEndVertex.id + ", " + mouse + ")");
      }
      cutPreviousPoint = mouse;
      cutPolyline.add(mouse);
    }
  }
}

void mouseReleased()
{
  if(cutting == true)
  {
    cutting = false;
    knifeDirection = null;
    cutPolyline = new ArrayList<PVector>();
    println("----------------");
  }
  
  previousCutEndVertex = null;
  
}

void keyPressed()
{
  switch(key)
  {
    case 'r':
      addRandomPointsToTriangulator();
      initPhysics();
      break;
    case 'd':
      if (doPhysics)
        physics.disturb();
      break;
    case 't':
      if(triangulator.vertices.size() > 2)
      {
        mesh = triangulator.triangulate();
	triangulator = new Triangulator();
        initPhysics();
      }
      break;
    case 'p':
      doPhysics = !doPhysics;
      println(doPhysics ? "Physics ON." : "Physics OFF.");
      break;
    case 'c':
      debugDrawMesh = !debugDrawMesh;
      break;
  }
}


void addRandomPointsToTriangulator()
{
  int lowesty = 10000000;
  List<Integer> points_x = new ArrayList<Integer>();
  List<Integer> points_y = new ArrayList<Integer>();
  for(int p = 0; p < NUMBER_OF_RANDOM_POINTS; p++)
  {
    int x = (int) random(0.1 * SCREENX, 0.9 * SCREENX);
    int y = (int) random(0.1 * SCREENY, 0.9 * SCREENY);
    if (y < lowesty) {
      lowesty = y;
    }
    points_x.add(x);
    points_y.add(y);
  }

  int diff = lowesty - 30;
  for(int p = 0; p < NUMBER_OF_RANDOM_POINTS; p++)
  {
    if (diff > 0) {
      triangulator.addPoint(points_x.get(p), points_y.get(p)-diff);
    }
    else {
      triangulator.addPoint(points_x.get(p), points_y.get(p));
    }
  }

}
