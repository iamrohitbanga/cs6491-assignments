int OFF_SET = 0;

// do the first segment of a whole Polyline cut
Vertex cutFromPointToPoint(Mesh mesh, PVector fromPoint, PVector toPoint)
{ 
  //The triangle index that contains the fromPoint
  int fromTriangle = -1;
  //The triangle index that contains the toPoint
  int toTriangle = -1;
  int[] order = new int[3];
  
  //Remember if the toPoint is outside
  boolean toPointOut = false;
  
  Point from = new Point(fromPoint.x,fromPoint.y);
  Point to = new Point(toPoint.x,toPoint.y);
  
  Point p1;
  Point p2;
  Point p3;
  
  //Check if the two points are located inside a triangle
  for(int triangleIndex = 0; triangleIndex < mesh.V.length / 3; triangleIndex++)
  {
    p1 = new Point(mesh.G[mesh.V[triangleIndex * 3 + 0]].x,mesh.G[mesh.V[triangleIndex * 3 + 0]].y);
    p2 = new Point(mesh.G[mesh.V[triangleIndex * 3 + 1]].x,mesh.G[mesh.V[triangleIndex * 3 + 1]].y);
    p3 = new Point(mesh.G[mesh.V[triangleIndex * 3 + 2]].x,mesh.G[mesh.V[triangleIndex * 3 + 2]].y);
    Triangle aTriangle = new Triangle(p1,p2,p3);
    if(aTriangle.contains(from)) fromTriangle = triangleIndex;
    if(aTriangle.contains(to)) toTriangle = triangleIndex;
  }
  
  //There are four scenarios:
  //1 and 2. fromPoint is outside a mesh and toPoint is inside a mesh, and vice versa.
  //3. fromPoint and toPoint are both inside a mesh
  //4. fromPoint and toPoint are ouside a mesh
  Point intersection = null;
  if(fromTriangle != -1 && toTriangle == -1)
  {
    //It's the same as fromPoint is outside and toPoint is inside
    PVector copyPoint = new PVector(toPoint.x,toPoint.y);
    toPoint = fromPoint;
    fromPoint = copyPoint;
    int copyInt = toTriangle;
    toTriangle = fromTriangle;
    fromTriangle = copyInt;
    toPointOut = true;
  }
  if(fromTriangle == -1 && toTriangle != -1)
  {
    p1 = new Point(mesh.G[mesh.V[toTriangle * 3 + 0]].x,mesh.G[mesh.V[toTriangle * 3 + 0]].y);
    p2 = new Point(mesh.G[mesh.V[toTriangle * 3 + 1]].x,mesh.G[mesh.V[toTriangle * 3 + 1]].y);
    p3 = new Point(mesh.G[mesh.V[toTriangle * 3 + 2]].x,mesh.G[mesh.V[toTriangle * 3 + 2]].y);
    
    //Check which edge intersects with the polyline
    if((intersection = getIntersection(p1,p2,from,to)) != null){
       p3 = null;
       order[0] = mesh.V[toTriangle * 3 + 1];
       order[1] = mesh.V[toTriangle * 3 + 2];
       order[2] = mesh.V[toTriangle * 3 + 0];
    }
    else if((intersection = getIntersection(p1,p3,from,to)) != null){
       p2 = null;
       order[0] = mesh.V[toTriangle * 3 + 0];
       order[1] = mesh.V[toTriangle * 3 + 1];
       order[2] = mesh.V[toTriangle * 3 + 2];
    }
    else{
      intersection = getIntersection(p2,p3,from,to);
       p1 = null;
       order[0] = mesh.V[toTriangle * 3 + 2];
       order[1] = mesh.V[toTriangle * 3 + 0];
       order[2] = mesh.V[toTriangle * 3 + 1];
    }
       
    
    Mesh newMesh = new Mesh();
    
    //Create new G
    newMesh.G = new PVector[mesh.G.length+3];
    for(int i=0; i< mesh.G.length; i++){
      newMesh.G[i]=mesh.G[i];
    }
    newMesh.G[newMesh.G.length-3] = new PVector(intersection.x,  intersection.y - OFF_SET);
    newMesh.G[newMesh.G.length-2] = toPoint;
    newMesh.G[newMesh.G.length-1] = new PVector(intersection.x,  intersection.y);
    
    //Create new V
    newMesh.V = new int[mesh.V.length+9];
    for(int i=0; i< mesh.V.length; i++){
      newMesh.V[i]=mesh.V[i];
    }
    
    newMesh.V[toTriangle * 3 + 0] = newMesh.G.length-2;
    newMesh.V[toTriangle * 3 + 1] = newMesh.G.length-3;
    newMesh.V[toTriangle * 3 + 2] = order[0];
    newMesh.V[newMesh.V.length-9] = newMesh.G.length-2;
    newMesh.V[newMesh.V.length-8] = order[0];
    newMesh.V[newMesh.V.length-7] = order[1];
    newMesh.V[newMesh.V.length-6] = newMesh.G.length-2;
    newMesh.V[newMesh.V.length-5] = order[1];
    newMesh.V[newMesh.V.length-4] = order[2];
    newMesh.V[newMesh.V.length-3] = newMesh.G.length-2;
    newMesh.V[newMesh.V.length-2] = order[2];
    newMesh.V[newMesh.V.length-1] = newMesh.G.length-1;
    
    //Create new C
    newMesh.C = new int[newMesh.G.length];
    for(int i=0; i< mesh.C.length; i++){
      newMesh.C[i]=mesh.C[i];
    }
    
    newMesh.C[newMesh.C.length-3] = toTriangle * 3 + 1;
    newMesh.C[newMesh.C.length-2] = toTriangle * 3 + 0;
    newMesh.C[newMesh.C.length-1] = newMesh.V.length-1;
    
    //Update C for each triangle point in the original triangle
    if(mesh.C[mesh.V[toTriangle * 3 + 0]] == toTriangle * 3 + 0){
     newMesh.C[mesh.V[toTriangle * 3 + 0]] = newMesh.V.length-8;
    }
    
    if(mesh.C[mesh.V[toTriangle * 3 + 2]] == toTriangle * 3 + 2){
     newMesh.C[mesh.V[toTriangle * 3 + 2]] = newMesh.V.length-2;
    }
    
    //Create new S
    newMesh.S = new int[newMesh.V.length];
    for(int i=0; i< mesh.S.length; i++){
      newMesh.S[i]=mesh.S[i];
    }
    
    newMesh.S[toTriangle * 3 + 0] = newMesh.S.length-9;
    newMesh.S[toTriangle * 3 + 1] = -1;
    newMesh.S[toTriangle * 3 + 2] = mesh.S[toTriangle * 3 + 0];
    newMesh.S[newMesh.S.length-9] = newMesh.S.length-6;
    newMesh.S[newMesh.S.length-8] = toTriangle * 3 + 2;
    newMesh.S[newMesh.S.length-7] = mesh.S[toTriangle * 3 + 1];
    newMesh.S[newMesh.S.length-6] = newMesh.S.length-3;
    newMesh.S[newMesh.S.length-5] = newMesh.S.length-7;
    newMesh.S[newMesh.S.length-4] = mesh.S[toTriangle * 3 + 2];
    newMesh.S[newMesh.S.length-3] = -1;
    newMesh.S[newMesh.S.length-2] = newMesh.S.length-4;
    newMesh.S[newMesh.S.length-1] = -1;
    
    //Update old S
    for(int i=0; i<mesh.S.length ; i++){
     if(mesh.S[i]==toTriangle * 3 + 0) 
       newMesh.S[i] = newMesh.S.length-8;
     if(mesh.S[i]==toTriangle * 3 + 1) 
       newMesh.S[i] = newMesh.S.length-5;
    }
    
    mesh.G = newMesh.G;
    mesh.V = newMesh.V;
    mesh.C = newMesh.C;
    mesh.S = newMesh.S;
    
    if(!toPointOut){
    return new Vertex(mesh,mesh.G.length-2);
    }
    
  }
  
  return null; 
}

// do a subsequent segment of a whole Polyline cut
Vertex cutFromVertexToPoint(Mesh mesh, Vertex fromVertex, PVector toPoint)
{
  println("work on vertex: "+ mesh.G[fromVertex.id].x + "," +mesh.G[fromVertex.id].y);
  //The triangle index that contains the toPoint
  int fromTriangle = -1;
  int toTriangle = -1;
  int[] order = new int[3];
  
  
  PVector fromVector = mesh.G[fromVertex.id];
  Point from = new Point(fromVector.x,fromVector.y);
  Point to = new Point(toPoint.x,toPoint.y);
  
  Point p1 = new Point(0,0);
  Point p2 = new Point(0,0);
  Point p3 = new Point(0,0);
  boolean crossEdge = true;
  
  
  //Check if the the second point crosses an edge
  for(int triangleIndex = 0; triangleIndex < mesh.V.length / 3; triangleIndex++)
  {
    p1 = new Point(mesh.G[mesh.V[triangleIndex * 3 + 0]].x,mesh.G[mesh.V[triangleIndex * 3 + 0]].y);
    p2 = new Point(mesh.G[mesh.V[triangleIndex * 3 + 1]].x,mesh.G[mesh.V[triangleIndex * 3 + 1]].y);
    p3 = new Point(mesh.G[mesh.V[triangleIndex * 3 + 2]].x,mesh.G[mesh.V[triangleIndex * 3 + 2]].y);
    Triangle aTriangle = new Triangle(p1,p2,p3);
    if(aTriangle.contains(to)){
      toTriangle = triangleIndex;
      break;
    }
  }
  if(toTriangle != -1){
    if((p1.x == fromVector.x && p1.y == fromVector.y) || (p2.x == fromVector.x && p2.y == fromVector.y) || (p3.x == fromVector.x && p3.y == fromVector.y))
    crossEdge = false;
  }
  
  Mesh newMesh = new Mesh();
  
  //There are three scenarios:
  //1. The second point does not cross an edge
  //2. The second point crosses an edge and is inside the mesh
  //3. The second point crosses an edge but is ouside the mesh
  Point intersection = null;
  if(!crossEdge)
  {
    println("Not Cross Edge");
    p1 = new Point(mesh.G[mesh.V[toTriangle * 3 + 0]].x,mesh.G[mesh.V[toTriangle * 3 + 0]].y);
    p2 = new Point(mesh.G[mesh.V[toTriangle * 3 + 1]].x,mesh.G[mesh.V[toTriangle * 3 + 1]].y);
    p3 = new Point(mesh.G[mesh.V[toTriangle * 3 + 2]].x,mesh.G[mesh.V[toTriangle * 3 + 2]].y);
    
    
    //Create new G
    newMesh.G = new PVector[mesh.G.length+2];
    for(int i=0; i< mesh.G.length; i++){
      newMesh.G[i]=mesh.G[i];
    }
    newMesh.G[newMesh.G.length-2] = toPoint;
    newMesh.G[newMesh.G.length-1] = new PVector(mesh.G[fromVertex.id].x,  mesh.G[fromVertex.id].y+OFF_SET);
    
    //Create new V
    newMesh.V = new int[mesh.V.length+6];
    for(int i=0; i< mesh.V.length; i++){
      newMesh.V[i]=mesh.V[i];
    }
    
    newMesh.V[toTriangle * 3 + 0] = newMesh.G.length-2;
    newMesh.V[toTriangle * 3 + 1] = mesh.V[toTriangle * 3 + 0];
    newMesh.V[toTriangle * 3 + 2] = mesh.V[toTriangle * 3 + 1];
    newMesh.V[newMesh.V.length-6] = newMesh.G.length-2;
    newMesh.V[newMesh.V.length-5] = mesh.V[toTriangle * 3 + 1];
    newMesh.V[newMesh.V.length-4] = mesh.V[toTriangle * 3 + 2];
    newMesh.V[newMesh.V.length-3] = newMesh.G.length-2;
    newMesh.V[newMesh.V.length-2] = mesh.V[toTriangle * 3 + 2];
    newMesh.V[newMesh.V.length-1] = newMesh.G.length-1;
    
    //Create new C
    newMesh.C = new int[newMesh.G.length];
    for(int i=0; i< mesh.C.length; i++){
      newMesh.C[i]=mesh.C[i];
    }
    
    newMesh.C[newMesh.C.length-2] = toTriangle * 3 + 0;
    newMesh.C[newMesh.C.length-1] = newMesh.V.length-1;
    
    //Update C for each triangle point in the original triangle
    if(mesh.C[mesh.V[toTriangle * 3 + 0]] == toTriangle * 3 + 0){
     newMesh.C[mesh.V[toTriangle * 3 + 0]] = newMesh.V.length-5;
    }
    
    if(mesh.C[mesh.V[toTriangle * 3 + 2]] == toTriangle * 3 + 2){
     newMesh.C[mesh.V[toTriangle * 3 + 2]] = newMesh.V.length-2;
    }
    if(mesh.C[mesh.V[toTriangle * 3 + 1]] == toTriangle * 3 + 1){
     newMesh.C[mesh.V[toTriangle * 3 + 1]] = newMesh.V.length-5;
    }
    println("success");
    //Create new S
    newMesh.S = new int[newMesh.V.length];
    for(int i=0; i< mesh.S.length; i++){
      newMesh.S[i]=mesh.S[i];
    }
    
    newMesh.S[toTriangle * 3 + 0] = newMesh.S.length-6;
    newMesh.S[toTriangle * 3 + 1] = -1;
    newMesh.S[toTriangle * 3 + 2] = mesh.S[toTriangle * 3 + 1];
    newMesh.S[newMesh.S.length-6] = newMesh.S.length-3;
    newMesh.S[newMesh.S.length-5] = toTriangle * 3 + 2;
    newMesh.S[newMesh.S.length-4] = mesh.S[toTriangle * 3 + 2];
    newMesh.S[newMesh.S.length-3] = -1;
    newMesh.S[newMesh.S.length-2] = newMesh.S.length-4;
    newMesh.S[newMesh.S.length-1] = mesh.S[toTriangle * 3 + 0];
    
    //Update one of the old triangles
    
    int id = toTriangle * 3 + 0;
    while((id = mesh.S[id]) != -1){
      newMesh.V[id] = newMesh.G.length-1; 
    }
    
    //Update old S
    for(int i=0; i<mesh.S.length ; i++){
     if(mesh.S[i]==toTriangle * 3 + 0) 
       newMesh.S[i] = newMesh.S.length-8;
     if(mesh.S[i]==toTriangle * 3 + 1) 
       newMesh.S[i] = newMesh.S.length-5;
     if(mesh.S[i]==toTriangle * 3 + 2) 
       newMesh.S[i] = newMesh.S.length-2;
    }
    
    mesh.G = newMesh.G;
    mesh.V = newMesh.V;
    mesh.C = newMesh.C;
    mesh.S = newMesh.S;
    
    
    return new Vertex(mesh,mesh.G.length-2);
  
  }
  else if(crossEdge && toTriangle != -1)
  {
    println("Cross Edge");
    p1 = new Point(mesh.G[mesh.V[toTriangle * 3 + 0]].x,mesh.G[mesh.V[toTriangle * 3 + 0]].y);
    p2 = new Point(mesh.G[mesh.V[toTriangle * 3 + 1]].x,mesh.G[mesh.V[toTriangle * 3 + 1]].y);
    p3 = new Point(mesh.G[mesh.V[toTriangle * 3 + 2]].x,mesh.G[mesh.V[toTriangle * 3 + 2]].y);
    
    //Check which edge intersects with the polyline
    if((intersection = getIntersection(p1,p2,from,to)) != null){
       p3 = null;
       order[0] = mesh.V[toTriangle * 3 + 1];
       order[1] = mesh.V[toTriangle * 3 + 2];
       order[2] = mesh.V[toTriangle * 3 + 0];
    }
    else if((intersection = getIntersection(p1,p3,from,to)) != null){
       p2 = null;
       order[0] = mesh.V[toTriangle * 3 + 0];
       order[1] = mesh.V[toTriangle * 3 + 1];
       order[2] = mesh.V[toTriangle * 3 + 2];
    }
    else{
      intersection = getIntersection(p2,p3,from,to);
       p1 = null;
       order[0] = mesh.V[toTriangle * 3 + 2];
       order[1] = mesh.V[toTriangle * 3 + 0];
       order[2] = mesh.V[toTriangle * 3 + 1];
    }
       
    
    Point mid = new Point(abs((intersection.x+mesh.G[fromVertex.id].x)/2),abs((intersection.y+mesh.G[fromVertex.id].y)/2));
    //Find the triangle the fromVertex is in
    for(int triangleIndex = 0; triangleIndex < mesh.V.length / 3; triangleIndex++)
    {
      p1 = new Point(mesh.G[mesh.V[triangleIndex * 3 + 0]].x,mesh.G[mesh.V[triangleIndex * 3 + 0]].y);
      p2 = new Point(mesh.G[mesh.V[triangleIndex * 3 + 1]].x,mesh.G[mesh.V[triangleIndex * 3 + 1]].y);
      p3 = new Point(mesh.G[mesh.V[triangleIndex * 3 + 2]].x,mesh.G[mesh.V[triangleIndex * 3 + 2]].y);
      Triangle aTriangle = new Triangle(p1,p2,p3);
      if(aTriangle.contains(mid)){
        fromTriangle = triangleIndex;
        break;
      }
    }
    
    //Create new G
    newMesh.G = new PVector[mesh.G.length+4];
    for(int i=0; i< mesh.G.length; i++){
      newMesh.G[i]=mesh.G[i];
    }
    newMesh.G[newMesh.G.length-4] = new PVector(intersection.x,  intersection.y - OFF_SET);
    newMesh.G[newMesh.G.length-3] = new PVector(toPoint.x, toPoint.y);
    newMesh.G[newMesh.G.length-2] = new PVector(intersection.x,  intersection.y+OFF_SET);
    newMesh.G[newMesh.G.length-1] = new PVector(mesh.G[fromVertex.id].x,  mesh.G[fromVertex.id].y+OFF_SET);
    println("success");
    //Create new V
    newMesh.V = new int[mesh.V.length+12];
    for(int i=0; i< mesh.V.length; i++){
      newMesh.V[i]=mesh.V[i];
    }
    
    newMesh.V[fromTriangle * 3 + 0] = newMesh.G.length-4;
    newMesh.V[fromTriangle * 3 + 1] = fromVertex.id;
    newMesh.V[fromTriangle * 3 + 2] = order[0];
    
    newMesh.V[toTriangle * 3 + 0] = newMesh.G.length-3;
    newMesh.V[toTriangle * 3 + 1] = newMesh.G.length-4;
    newMesh.V[toTriangle * 3 + 2] = order[0];
    newMesh.V[newMesh.V.length-12] = newMesh.G.length-3;
    newMesh.V[newMesh.V.length-11] = order[0];
    newMesh.V[newMesh.V.length-10] = order[1];
    newMesh.V[newMesh.V.length-9] = newMesh.G.length-3;
    newMesh.V[newMesh.V.length-8] = order[1];
    newMesh.V[newMesh.V.length-7] = order[2];
    newMesh.V[newMesh.V.length-6] = newMesh.G.length-3;
    newMesh.V[newMesh.V.length-5] = order[2];
    newMesh.V[newMesh.V.length-4] = newMesh.G.length-2;
    
    newMesh.V[newMesh.V.length-3] = newMesh.G.length-2;
    newMesh.V[newMesh.V.length-2] = order[2];
    newMesh.V[newMesh.V.length-1] = newMesh.G.length-1;
    
    //Create new C
    newMesh.C = new int[newMesh.G.length];
    for(int i=0; i< mesh.C.length; i++){
      newMesh.C[i]=mesh.C[i];
    }
    
    newMesh.C[newMesh.C.length-4] = fromTriangle * 3 + 0;
    newMesh.C[newMesh.C.length-3] = toTriangle * 3 + 0;
    newMesh.C[newMesh.C.length-2] = newMesh.V.length-4;
    newMesh.C[newMesh.C.length-1] = newMesh.V.length-1;
    
    //Update C for each triangle point in the original triangle
    if(mesh.C[mesh.V[toTriangle * 3 + 0]] == toTriangle * 3 + 0){
     newMesh.C[mesh.V[toTriangle * 3 + 0]] = newMesh.V.length-11;
    }
    
    if(mesh.C[mesh.V[toTriangle * 3 + 1]] == toTriangle * 3 + 1){
     newMesh.C[mesh.V[toTriangle * 3 + 1]] = newMesh.V.length-8;
    }
    
    //Create new S
    newMesh.S = new int[newMesh.V.length];
    for(int i=0; i< mesh.S.length; i++){
      newMesh.S[i]=mesh.S[i];
    }
    
    newMesh.S[fromTriangle * 3 + 0] = toTriangle * 3 + 1;
    newMesh.S[fromTriangle * 3 + 1] = -1;
    newMesh.S[fromTriangle * 3 + 2] = mesh.S[fromTriangle * 3 + 1];
    
    newMesh.S[toTriangle * 3 + 0] = newMesh.S.length-12;
    newMesh.S[toTriangle * 3 + 1] = -1;
    newMesh.S[toTriangle * 3 + 2] = fromTriangle * 3 + 2;
    newMesh.S[newMesh.S.length-12] = newMesh.S.length-9;
    newMesh.S[newMesh.S.length-11] = toTriangle * 3 + 2;
    newMesh.S[newMesh.S.length-10] = mesh.S[toTriangle * 3 + 1];
    newMesh.S[newMesh.S.length-9] = newMesh.S.length-6;
    newMesh.S[newMesh.S.length-8] = newMesh.S.length-10;
    newMesh.S[newMesh.S.length-7] = mesh.S[toTriangle * 3 + 2];
    newMesh.S[newMesh.S.length-6] = -1;
    newMesh.S[newMesh.S.length-5] = newMesh.S.length-7;
    newMesh.S[newMesh.S.length-4] = newMesh.S.length-3;
    
    newMesh.S[newMesh.S.length-3] = -1;
    newMesh.S[newMesh.S.length-2] = newMesh.S.length-5;
    newMesh.S[newMesh.S.length-1] = mesh.S[fromTriangle * 3 + 0];
    
    //Update one of the old triangles
    int id = fromTriangle * 3 + 0;
    while((id = mesh.S[id]) != -1){
      newMesh.V[id] = newMesh.G.length-1; 
    }
    
    //Update old S
    for(int i=0; i<mesh.S.length ; i++){
     if(mesh.S[i]==fromTriangle * 3 + 0) 
       newMesh.S[i] = fromTriangle * 3 + 1;
     if(mesh.S[i]==fromTriangle * 3 + 2) 
       newMesh.S[i] = newMesh.S.length-2;
     if(mesh.S[i]==toTriangle * 3 + 0) 
       newMesh.S[i] = newMesh.S.length-11;
     if(mesh.S[i]==toTriangle * 3 + 1) 
       newMesh.S[i] = newMesh.S.length-8;
    }
    
    
    mesh.G = newMesh.G;
    mesh.V = newMesh.V;
    mesh.C = newMesh.C;
    mesh.S = newMesh.S;
    
    return new Vertex(mesh,mesh.G.length-3);
  }
  else
  {
    println("In to Out");
    
    float stepX = (to.x - from.x)/CUT_VERTEX_DISTANCE;
    float stepY = (to.y - from.y)/CUT_VERTEX_DISTANCE;
    Point aPoint = new Point(0,0);
    toTriangle = -1;
    println("success");
    //Check which triangle the line cut through
   for(int triangleIndex = 0; triangleIndex < mesh.V.length / 3; triangleIndex++)
  {
    p1 = new Point(mesh.G[mesh.V[triangleIndex * 3 + 0]].x,mesh.G[mesh.V[triangleIndex * 3 + 0]].y);
    p2 = new Point(mesh.G[mesh.V[triangleIndex * 3 + 1]].x,mesh.G[mesh.V[triangleIndex * 3 + 1]].y);
    p3 = new Point(mesh.G[mesh.V[triangleIndex * 3 + 2]].x,mesh.G[mesh.V[triangleIndex * 3 + 2]].y);
    
    for(int i=0;i<CUT_VERTEX_DISTANCE;i++)
    {
      aPoint = new Point(toPoint.x - stepX * i,toPoint.y - stepY * i);
      Triangle aTriangle = new Triangle(p1,p2,p3);
      if(aTriangle.contains(aPoint)){
        toTriangle = triangleIndex;
        break;
      }
    }
    
    if(toTriangle != -1) break;
   }
    
    p1 = new Point(mesh.G[mesh.V[toTriangle * 3 + 0]].x,mesh.G[mesh.V[toTriangle * 3 + 0]].y);
    p2 = new Point(mesh.G[mesh.V[toTriangle * 3 + 1]].x,mesh.G[mesh.V[toTriangle * 3 + 1]].y);
    p3 = new Point(mesh.G[mesh.V[toTriangle * 3 + 2]].x,mesh.G[mesh.V[toTriangle * 3 + 2]].y);
    
    //Check which edge intersects with the polyline
    if((intersection = getIntersection(p1,p2,aPoint,to)) != null){
    }
    else if((intersection = getIntersection(p1,p3,aPoint,to)) != null){
    }
    else{
      intersection = getIntersection(p2,p3,aPoint,to);
    }
    
    
    //Create new G
    newMesh.G = new PVector[mesh.G.length+3];
    for(int i=0; i< mesh.G.length; i++){
      newMesh.G[i]=mesh.G[i];
    }
   
    newMesh.G[newMesh.G.length-3] = new PVector(intersection.x,  intersection.y);
    newMesh.G[newMesh.G.length-2] = new PVector(intersection.x,  intersection.y+OFF_SET);
    newMesh.G[newMesh.G.length-1] = new PVector(mesh.G[fromVertex.id].x,  mesh.G[fromVertex.id].y+OFF_SET);
    
    //Create new V
    newMesh.V = new int[mesh.V.length+3];
    for(int i=0; i< mesh.V.length; i++){
      newMesh.V[i]=mesh.V[i];
    }
    
    newMesh.V[toTriangle * 3 + 0] = newMesh.G.length-3;
    newMesh.V[toTriangle * 3 + 1] = mesh.V[toTriangle * 3 + 0];
    newMesh.V[toTriangle * 3 + 2] = mesh.V[toTriangle * 3 + 1];
    newMesh.V[newMesh.V.length-3] = newMesh.G.length-2;
    newMesh.V[newMesh.V.length-2] = mesh.V[toTriangle * 3 + 2];
    newMesh.V[newMesh.V.length-1] = newMesh.G.length-1;
    
    //Create new C
    newMesh.C = new int[newMesh.G.length];
    for(int i=0; i< mesh.C.length; i++){
      newMesh.C[i]=mesh.C[i];
    }
    
    newMesh.C[newMesh.C.length-3] = toTriangle * 3 + 0;
    newMesh.C[newMesh.C.length-2] = newMesh.V.length-3;
    newMesh.C[newMesh.C.length-1] = newMesh.V.length-1;
    
    //Update C for each triangle point in the original triangle
    if(mesh.C[mesh.V[toTriangle * 3 + 0]] == toTriangle * 3 + 0){
     newMesh.C[mesh.V[toTriangle * 3 + 0]] = toTriangle * 3 + 2;
    }
    
      //Create new S
      newMesh.S = new int[newMesh.V.length];
      for(int i=0; i< mesh.S.length; i++){
        newMesh.S[i]=mesh.S[i];
      }
      
      newMesh.S[toTriangle * 3 + 0] = -1;
      newMesh.S[toTriangle * 3 + 1] = -1;
      newMesh.S[toTriangle * 3 + 2] = mesh.S[toTriangle * 3 + 1];
      newMesh.S[newMesh.S.length-3] = -1;
      newMesh.S[newMesh.S.length-2] = -1;
      newMesh.S[newMesh.S.length-1] = mesh.S[toTriangle * 3 + 0];
      
      //Update one of the old triangles
      
      int id = toTriangle * 3 + 0;
    while((id = mesh.S[id]) != -1){
      newMesh.V[id] = newMesh.G.length-1; 
    }
    
    //Update old S
    for(int i=0; i<mesh.S.length ; i++){
     if(mesh.S[i]==toTriangle * 3 + 0) 
       newMesh.S[i] = toTriangle * 3 + 1;
     if(mesh.S[i]==toTriangle * 3 + 2) 
       newMesh.S[i] = newMesh.S.length-2;
    }
    
    
    mesh.G = newMesh.G;
    mesh.V = newMesh.V;
    mesh.C = newMesh.C;
    mesh.S = newMesh.S;
    
  }

  return null;
}

Point getIntersection(Point point1,Point point2,Point point3,Point point4) 
{
  float x1 = point1.x;
  float y1 = point1.y;
  float x2 = point2.x;
  float y2 = point2.y;
  float x3 = point3.x;
  float y3 = point3.y;
  float x4 = point4.x;
  float y4 = point4.y;
  float d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
  if (d == 0) return null;
  
  float xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/d;
  float yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/d;
  if(!(xi >= x3 && xi <= x4) && !(xi <= x3 && xi >= x4)) return null;
  
  return new Point(xi,yi);
}
