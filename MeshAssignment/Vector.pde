class Vector
{
  float x, y;
  public Vector(Point from, Point to)
  {
    x = to.x - from.x;
    y = to.y - from.y;
  }

  public Vector(float x, float y)
  {
    this.x = x;
    this.y = y;
  }

  public Vector(float x1, float y1, float x2, float y2)
  {
    this.x = x2 - x1;
    this.y = y2 - y1;
  }

  Vector scaled(float scale)
  {
    return new Vector(scale * this.x, scale * this.y);
  }

  Vector rotated(float angle)
  {
    float x = magnitude() * cos(this.angle() + angle);
    float y = magnitude() * sin(this.angle() + angle);
    return new Vector(x, y);
  }

  Vector normalized()
  {
    return new Vector(this.x / this.magnitude(), this.y / this.magnitude());
  }

  Vector plus(Vector other)
  {
    return new Vector(this.x + other.x, this.y + other.y);
  }

  float magnitude()
  {
    return sqrt(sq(this.x) + sq(this.y));
  }

  float angle()
  {
    return atan2(this.y, this.x);
  }

  float dot(Vector other)
  {
    return this.x * other.x + this.y * other.y;
  }

  float angleTo(Vector other)
  {
    //float numerator = this.dot(other);
    //float denominator = this.magnitude() * other.magnitude();
    //return acos(numerator / denominator);
    return other.angle() - this.angle();
  }
}

int crossParity(Vector v1, Vector v2)
{
  return v1.x * v2.y - v1.y * v2.x >= 0 ? 1 : -1;
}
